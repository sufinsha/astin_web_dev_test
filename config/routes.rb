Rails.application.routes.draw do
  # get 'addresses/get_address'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"

  scope '/myapp' do
    get 'get_address/:lat/:long', to: 'addresses#fetch_address',
                                  constraints: {
                                    lat: /[+-]*\d+[.]*\d+/,
                                    long: /[+-]*\d+[.]*\d+/
                                  },
                                  defaults: { format: :json }
  end
end
