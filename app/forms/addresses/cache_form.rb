module Addresses
  # CacheForm
  class CacheForm
    include ActiveModel::Model

    attr_reader :lat, :long, :api_response, :address_obj

    validates :lat, presence: true
    validates :long, presence: true
    validates :api_response, presence: true

    def initialize(params)
      @lat = params[:lat]
      @long = params[:long]
      @api_response = params[:api_response]
    end

    def save
      @address_obj = Address.where(lat: lat, long: long).first_or_initialize
      @address_obj.response = api_response
      @address_obj.cached_at = DateTime.current

      @address_obj.save if valid?
    end
  end
end
