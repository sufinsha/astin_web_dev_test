require 'nominatim/reverse_geocode'

module Addresses
  # Fetch Address Service
  class FetchAddressService
    attr_reader :lat, :long

    def initialize(lat, long)
      @lat = lat
      @long = long
    end

    def call
      return if lat.blank? && long.blank?

      fetch_from_db { return @address }

      fetch_from_api
      save_api_response_to_db { return @address }
    end

    def fetch_from_db
      address_obj = Addresses::Query.find_first(lat: lat, long: long)

      return if address_obj.blank? || address_obj.expired?(SETTINGS[:address_cache_expiry_seconds])

      @address = address_obj&.address

      yield
    end

    def fetch_from_api
      @api_response = ::Nominatim::ReverseGeocode.new(lat, long).call
    end

    def save_api_response_to_db
      return if @api_response.blank?

      address_form = Addresses::CacheForm.new(lat: lat, long: long, api_response: @api_response)
      address_form.save

      return if address_form.errors.any?

      @address = address_form.address_obj.address

      yield
    end

    def cache_key
      "(#{lat},#{long})"
    end
  end
end
