module Addresses
  class Query
    class << self
      def find_first(params)
        Address.where(params).first
      end
    end
  end
end
