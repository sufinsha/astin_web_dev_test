class Address < ApplicationRecord
  serialize :response, Hash

  def address
    (response || {}).try(:[], 'display_name')
  end

  def expired?(expiry_in_seconds)
    (DateTime.current - cached_at.to_datetime).to_f * 24 * 60 * 60 > expiry_in_seconds
  end
end
