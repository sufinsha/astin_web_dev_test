class AddressesController < ApplicationController
  def fetch_address
    @address = Addresses::FetchAddressService.new(
      fetch_address_params[:lat], fetch_address_params[:long]
    ).call
  end

  private

  def fetch_address_params
    params.permit(:format, :lat, :long)
  end
end
