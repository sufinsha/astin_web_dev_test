class CreateAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :addresses do |t|
      t.decimal :lat
      t.decimal :long
      t.text :response
      t.datetime :cached_at

      t.timestamps
    end
  end
end
