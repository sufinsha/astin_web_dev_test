require 'test_helper'
require 'nominatim/nominatim'

NOMINATIM_API_TEST_RESPONSE_1 = {
  'place_id' => 285_161_628,
  'licence' => 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
  'osm_type' => 'relation',
  'osm_id' => 3_722_476,
  'lat' => '10.037171950000001',
  'lon' => '10.561123309810402',
  'display_name' => 'Pali East, Alkaleri, Bauchi, Nigeria',
  'address' => {
    'city' => 'Pali East',
    'county' => 'Alkaleri',
    'state' => 'Bauchi',
    'ISO3166-2-lvl4' => 'NG-BA',
    'country' => 'Nigeria',
    'country_code' => 'ng'
  },
  'boundingbox' => ['9.8164783', '10.2574158', '10.395398', '10.647194']
}.freeze

NOMINATIM_API_TEST_RESPONSE_2 = {
  'place_id' => 285_161_628,
  'licence' => 'Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright',
  'osm_type' => 'relation',
  'osm_id' => 3_722_476,
  'lat' => '10.037171950000001',
  'lon' => '10.561123309810402',
  'display_name' => 'Test Address',
  'address' => {
    'city' => 'Pali East',
    'county' => 'Alkaleri',
    'state' => 'Bauchi',
    'ISO3166-2-lvl4' => 'NG-BA',
    'country' => 'Nigeria',
    'country_code' => 'ng'
  },
  'boundingbox' => ['9.8164783', '10.2574158', '10.395398', '10.647194']
}.freeze

def stub_nominatim_request(response, lat, long)
  stub_request(
    :get, "https://nominatim.openstreetmap.org/reverse?format=json&lat=#{lat}&lon=#{long}"
  ).with(
    headers: {
      'Accept' => '*/*',
      'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
      'Host' => 'nominatim.openstreetmap.org',
      'User-Agent' => 'rest-client/2.1.0 (linux x86_64) ruby/3.0.3p157'
    }
  ).to_return(status: 200, body: response, headers: {})
end

module Addresses
  class GetAddressTest < ActionDispatch::IntegrationTest
    test '
      A value which is not present in DB will trigger a call to Nominatim and a
      A subsequent call for same value wont trigger a call to Nominatim
    ' do
      lat = 9.999
      long = 10.545

      address_obj = Address.find_by(lat: lat, long: long)
      assert_equal false, address_obj.present?

      stub_nominatim_request(NOMINATIM_API_TEST_RESPONSE_1.to_json, lat, long)

      get "/myapp/get_address/#{lat}/#{long}"

      # Ensures - A value which is not present in DB will trigger a call to Nominatim
      assert_match 'Pali East, Alkaleri, Bauchi, Nigeria', @response.body

      stub_nominatim_request('{}', lat, long)

      get "/myapp/get_address/#{lat}/#{long}"

      # Ensures A subsequent call for same value wont trigger a call to Nominatim
      assert_match 'Pali East, Alkaleri, Bauchi, Nigeria', @response.body
    end

    test 'A value which is not present in DB will trigger a call to Nominatim' do
      lat = 10.999
      long = 10.545

      created_at_timestamp = DateTime.current - 2.days

      address_obj = Address.create(
        lat: lat, long: long, cached_at: created_at_timestamp,
        created_at: created_at_timestamp, updated_at: created_at_timestamp,
        response: NOMINATIM_API_TEST_RESPONSE_1
      )
      assert_match 'Pali East, Alkaleri, Bauchi, Nigeria', address_obj.address

      stub_nominatim_request(NOMINATIM_API_TEST_RESPONSE_2.to_json, lat, long)

      get "/myapp/get_address/#{lat}/#{long}"

      assert_match 'Test Address', @response.body

      address_obj = Address.find_by(lat: lat, long: long)
      assert_match 'Test Address', address_obj.address
    end
  end
end
