# astin_web_dev_test

## Prerequisites
- docker
- docker-compose

## Technology Stack
- Ruby on Rails
- DB: Sqlite

## Setup
- `cd docker`
- `docker-compose build bootstrap`

##  Start Development Server
- `cd docker`
- `docker-compose up web`

## Run Test Cases
- `cd docker`
- `docker-compose build web`
- `docker-compose up test`
