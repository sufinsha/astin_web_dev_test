#!/bin/bash
set -e

# Remove a potentially pre-existing server.pid for Rails.
rm -f /app/tmp/pids/server.pid

if [ "$RAILS_ENV" = "test" ]; then
  RAILS_ENV=test bundle exec rails db:reset
  RAILS_ENV=test rails test /app/test/integration/addresses/get_address_test.rb
fi

if [ "$APP_ROLE" = "wait_infinitely" ]; then
  tail -f /dev/null
fi

if [ "$APP_ROLE" = "bootstrap" ]; then
  bundle exec rails db:create
  bundle exec rails db:migrate
fi

if [ "$APP_ROLE" = "web" ]; then
  bundle exec rails db:migrate
  bundle exec rails s -p 3000 -b '0.0.0.0'
fi
