require 'external_apis/json_api'
require 'nominatim/nominatim'

module Nominatim
  class ReverseGeocode
    attr_reader :lat, :long

    def initialize(lat, long)
      @lat = lat
      @long = long
    end

    def call
      ExternalApis::JsonApi.get(
        REVERSE_URL, { lat: lat, lon: long, format: :json }
      )
    end
  end
end
