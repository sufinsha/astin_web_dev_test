module ExternalApis
  # Json Api Service
  class JsonApi
    class << self
      def get(url, params)
        request = RestClient.get(url, { params: params })

        @api_response = JSON.parse(request.body)
      rescue StandardError => e
        p e
        @api_response = {}
      end
    end
  end
end
